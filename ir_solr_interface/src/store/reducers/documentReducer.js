const initialState = {
  documents: []
};

const documentReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DOCUMENTS_SUCCESS':
      return ({
        ...state,
        documents: action.payload
      });
    default:
      return state;
  }
};

export default documentReducer;
