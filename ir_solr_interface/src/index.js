import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, createStore, compose } from 'redux';
import { Provider } from 'react-redux';
// import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import reducer from './store/reducers';
import rootSaga from './store/sagas';

import './index.css';
import App from './App';


// eslint-disable-next-line no-undef,no-underscore-dangle
const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const sagaMiddleware = createSagaMiddleware();
let composedMiddleware = applyMiddleware(sagaMiddleware, /* logger */);
if (reduxDevTools) {
  composedMiddleware = compose(applyMiddleware(sagaMiddleware, /* logger */), reduxDevTools);
}
// eslint-disable-next-line import/prefer-default-export
export const store = createStore(
  reducer, composedMiddleware
);

// Register all sagas;
rootSaga.map(saga => sagaMiddleware.run(saga));


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,

  // eslint-disable-next-line no-undef
  document.getElementById('root')
);
