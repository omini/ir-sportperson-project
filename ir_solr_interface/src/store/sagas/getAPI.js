import { put } from 'redux-saga/effects';
import getDocumentAPI from '../API';
import { getDocumentSuccess } from '../actions';


export default function* getDocumentsData(payload) {
  // console.log(payload.payload);
  try {
    // format the string in the right input for OR
    const res = yield getDocumentAPI(payload.payload);
    yield put(getDocumentSuccess(res.data));
  } catch (e) {
    console.log(e);
  }
}
