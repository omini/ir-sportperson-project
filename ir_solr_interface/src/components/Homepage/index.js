import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import Homepage from './Homepage';

const mapStateToProps = state => ({
  documents: state.documents.documents,
  content: state.documents
});

export default connect(mapStateToProps, actions)(Homepage);
