import axios from 'axios';

const getDocumentAPI = payload => axios.get(`/solr/sportPerson2/select?hl.fragsize=450&hl.fl=content&hl=on&q=content%3A(${payload})`);

// /solr/sportPerson/select?hl.fl=content&hl=on&q=content%3A%22serena%20williams%22

export default getDocumentAPI;
