export const getDocuments = payload => ({
  type: 'GET_DOCUMENTS',
  payload
});

export const getDocumentSuccess = payload => ({
  type: 'GET_DOCUMENTS_SUCCESS',
  payload
});

export const getDocumentsError = () => ({
  type: 'GET_DOCUMENTS_Error'
});
