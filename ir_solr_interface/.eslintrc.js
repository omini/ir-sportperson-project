module.exports = {
  extends: ['airbnb',
    "plugin:flowtype/recommended"],
  plugins: [
    'react',
    'babel',
    'flowtype'
  ],
  "parserOptions": {
    "ecmaVersion": 9,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true,
    }
  },
  "settings": {
    "flowtype": {
      "onlyFilesWithFlowAnnotation": true
    },
    "propWrapperFunctions": [
      // The names of any function used to wrap propTypes, e.g. `forbidExtraProps`. If this isn't set, any propTypes wrapped in a function will be skipped.
      "forbidExtraProps",
      {"property": "freeze", "object": "Object"},
      {"property": "myFavoriteWrapper"}
    ],
    "linkComponents": [
      // Components used as alternatives to <a> for linking, eg. <Link to={ url } />
      "Hyperlink",
      {"name": "Link", "linkAttribute": "to"}
    ]
  },

  rules: {
    "key-spacing": [
      "error",
      {
        "afterColon": true
      }
    ],
    "no-underscore-dangle": [
      "error",
      {
        "allowAfterThis": true
      }
    ],
    "no-use-before-define": [
      "error",
      {
        "variables": false
      }
    ],
    "jsx-quotes": [
      2,
      "prefer-single"
    ],
    "max-len": [
      "error",
      {
        "code": 120,
        "tabWidth": 2,
        "ignoreUrls": true,
        "ignoreRegExpLiterals": true
      }
    ],
    "semi": [
      "error",
      "always",
      {
        "omitLastInOneLineBlock": true
      }
    ],
    "curly": [
      "error",
      "all"
    ],
    "object-curly-spacing": [
      2,
      "always"
    ],
    "comma-dangle": [
      "error",
      "only-multiline"
    ],
    "consistent-return": "off",
    "arrow-parens": [
      "error",
      "as-needed"
    ],
    "react/jsx-filename-extension": [1, {"extensions": [".js", ".jsx"]}],
    "react/destructuring-assignment": [
      "error",
      "always",
      {
        "ignoreClassFields": true
      }
    ],
    "import/no-extraneous-dependencies": [
      "off"
    ],
    "jest/no-jasmine-globals": [
      "off"
    ],
    "no-param-reassign": "off",
  }
};
