// @flow
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import Fab from '@material-ui/core/Fab';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import NavigationIcon from '@material-ui/icons/Navigation';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';
import Chip from '@material-ui/core/Chip';
import FullScreenDialog from './dialog';


const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 500,
  },
  margin: {
    margin: theme.spacing(2.5),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  root: {
    padding: theme.spacing(3, 2, 3, 5),
    textAlign: 'left',
    margin: theme.spacing(3),
  },
  content: {
    margin: theme.spacing(3, 3, 0, 0),
    padding: theme.spacing(0)
  },
  infoButton: {
    position: 'absolute',
    top: '10px',
    right: '10px'
  }
}));

type Props = {
  documents: any,
  content: any,
  getDocuments: any
};

const Homepage = ({ documents, getDocuments, content }: Props) => {
  const classes = useStyles();
  const [inputValue, setInputValue] = useState('');
  const [projectInfo, setProjectInfo] = useState(false);

  const handleDialog = bool => {
    setProjectInfo(bool);
  };

  if (documents.response === undefined || documents.response.docs === []) {
    return (
      <div>
        <h1>Athlete Search</h1>
        <TextField
          id='outlined-basic'
          className={classes.textField}
          label='Search famous sportPerson'
          margin='normal'
          variant='outlined'
          autoFocus
          onChange={e => setInputValue(e.target.value)}
          onKeyPress={e => {
            if (inputValue !== '' && e.key === 'Enter') {
              getDocuments(inputValue);
              e.preventDefault();
            }
          }}
          value={inputValue}
        />
        <Fab
          variant='extended'
          color='primary'
          aria-label='add'
          className={classes.margin}
          onClick={() => { if (inputValue !== '') { getDocuments(inputValue) } }}
        >
          <NavigationIcon
            className={classes.extendedIcon}
          />
          Search
        </Fab>
        <Button variant='contained' className={classes.infoButton} onClick={e => handleDialog(true)}>
          Info
        </Button>
        {projectInfo ? <FullScreenDialog isOpen={projectInfo} setIsOpen={setProjectInfo} /> : null}
      </div>
    );
  }

  const dc = documents.response.docs;
  if (dc.length === 0) {
    const message = `${inputValue} ouputs no result`;
    return (
      <div>
        <TextField
          id='outlined-basic'
          className={classes.textField}
          label='Search famous sportPerson'
          margin='normal'
          variant='outlined'
          onChange={e => setInputValue(e.target.value)}
          value={inputValue}
          onKeyPress={e => {
            if (inputValue !== '' && e.key === 'Enter') {
              getDocuments(inputValue);
              e.preventDefault();
            }
          }}
        />
        <Fab
          variant='extended'
          color='primary'
          aria-label='add'
          className={classes.margin}
          onClick={() => { if (inputValue !== '') { getDocuments(inputValue) } }}
        >
          <NavigationIcon
            className={classes.extendedIcon}
          />
          Search
        </Fab>
        <Button variant='contained' className={classes.infoButton} onClick={e => handleDialog(true)}>
          Info
        </Button>
        {projectInfo ? <FullScreenDialog isOpen={projectInfo} setIsOpen={setProjectInfo} /> : null}
        <h3>
          { message }
        </h3>
      </div>
    );
  }
  return (
    <div>
      <TextField
        id='outlined-basic'
        className={classes.textField}
        label='Search famous sportPerson'
        margin='normal'
        variant='outlined'
        onChange={e => setInputValue(e.target.value)}
        value={inputValue}
        onKeyPress={e => {
          if (inputValue !== '' && e.key === 'Enter') {
            getDocuments(inputValue);
            e.preventDefault();
          }
        }}
      />
      <Fab
        variant='extended'
        color='primary'
        aria-label='add'
        className={classes.margin}
        onClick={() => { if (inputValue !== '') { getDocuments(inputValue) } }}
      >
        <NavigationIcon
          className={classes.extendedIcon}
        />
        Search
      </Fab>
      <Button variant='contained' className={classes.infoButton} onClick={e => handleDialog(true)}>
          Info
      </Button>
      {projectInfo ? <FullScreenDialog isOpen={projectInfo} setIsOpen={setProjectInfo} /> : null}
      {dc.map(doc => {
        // let bodyDoc = 0;

        // bodyDoc = Object.keys(content.documents.highlighting).filter(key => key === doc.url);
        let prova = doc.content;
        prova = prova.split(/[.\s-+=,:;&"']/);
        let unusedWord = inputValue;
        unusedWord = unusedWord.split(' ');
        for (let i = 0; i < prova.length; i += 1) {
          let j = 0;
          if (unusedWord.length === 0) {
            break;
          }
          while (j < unusedWord.length) {
            if (unusedWord[j].toLowerCase() === prova[i].toLowerCase()) {
              unusedWord.splice(j, 1);
            }
            j += 1;
          }
        }
        const displayUnuesedWord = `Missing: ${unusedWord.length === 1 ? unusedWord[0] : unusedWord.length} | Must include: ${unusedWord.length === 1 ? unusedWord[0] : unusedWord.length}`
        return (
          <Paper key={doc.id} className={classes.root}>
            <Link href={doc.url} target='_blank' rel='noopener'>
              <Typography variant='h5' component='h3'>
                {doc.title}
              </Typography>
            </Link>
            <Link href={doc.url} color='textSecondary' target='_blank' rel='noopener'>
              <Typography variant='body1' component='p'>
                {doc.url}
              </Typography>
            </Link>
            {(content.documents.highlighting[doc.url].content !== 'undefined')
              ? (
                <Container className={classes.content}>

                  <Typography component='div' className='snippet'>
                    <div dangerouslySetInnerHTML={
                      {
                        __html: content.documents.highlighting[doc.url].content
                      }
                      }
                    />
                  </Typography>
                </Container>
              ) : <h1>No content </h1> }
            {unusedWord.length !== 0
              ? <Chip size='small' label={displayUnuesedWord} onClick={() => getDocuments(`+(${String(unusedWord)}) ${inputValue}`)} />
              : null }
          </Paper>
        );
      })}
    </div>
  );
};

export default Homepage;
