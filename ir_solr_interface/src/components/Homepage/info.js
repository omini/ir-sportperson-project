import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    marginLeft: '20vw',
    marginRight: '20vw'
  }
}));

const Info = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <h1 className="code-line" data-line-start={0} data-line-end={1}>Information Retrieval Project</h1>
      <p className="has-line-data" data-line-start={1} data-line-end={3}>Author: Luca Omini<br />
        Professor: Fabio Crestani</p>
      <h4 className="code-line" data-line-start={4} data-line-end={5}>Run the frontend application</h4>
      <h5 className="code-line" data-line-start={6} data-line-end={7}>Install Yarn</h5>
      <p className="has-line-data" data-line-start={8} data-line-end={9}>If you don’t have yarn installed on your machine, you can find the installation instructions <a href="https://yarnpkg.com/en/docs/install#debian-stable">here</a></p>
      <h5 className="code-line" data-line-start={10} data-line-end={11}>Available Scripts</h5>
      <h6 className="code-line" data-line-start={11} data-line-end={12}><code>./startIR</code></h6>
      <p className="has-line-data" data-line-start={12} data-line-end={14}>In the main directory you will find a executable startIR, run it to start autoamtically solr on <a href="http://localhost:8983">http://localhost:8983</a> and the frontend apllication on <a href="http://localhost:300">http://localhost:3000</a>.<br />
        Be aware to have installed yarn and to have java version 8.</p>
      <p className="has-line-data" data-line-start={16} data-line-end={17}>In the frontend directory, you can run:</p>
      <h6 className="code-line" data-line-start={18} data-line-end={19}><code>yarn start</code></h6>
      <p className="has-line-data" data-line-start={20} data-line-end={22}>Runs the app in the development mode.<br />
        Open <a href="http://localhost:3000">http://localhost:3000</a> to view it in the browser.</p>
      <h6 className="code-line" data-line-start={23} data-line-end={24}><code>yarn test</code></h6>
      <p className="has-line-data" data-line-start={25} data-line-end={27}>Launches the test runner in the interactive watch mode.<br />
        See the section about <a href="https://facebook.github.io/create-react-app/docs/running-tests">running tests</a> for more information.</p>
      <h6 className="code-line" data-line-start={28} data-line-end={29}><code>yarn build</code></h6>
      <p className="has-line-data" data-line-start={30} data-line-end={35}>Builds the app for production to the <code>build</code> folder.<br />
        It correctly bundles React in production mode and optimizes the build for the best performance.<br />
        The build is minified and the filenames include the hashes.<br />
        Your app is ready to be deployed!<br />
        See the section about <a href="https://facebook.github.io/create-react-app/docs/deployment">deployment</a> for more information.</p>
      <h5 className="code-line" data-line-start={37} data-line-end={38}>Run the solr system</h5>
      <p className="has-line-data" data-line-start={39} data-line-end={40}>Navigate to the solr folder.</p>
      <h6 className="code-line" data-line-start={41} data-line-end={42}><code>./bin/solr start</code></h6>
      <p className="has-line-data" data-line-start={43} data-line-end={45}>Launches the solr application <a href="htttp://localhost:8983">htttp://localhost:8983</a>, you can access to the core SportPerson to get more<br />
        information and play with the query.</p>
      <h4 className="code-line" data-line-start={47} data-line-end={48}>Cheatsheet of all commands used in this project</h4>
      <p className="has-line-data" data-line-start={48} data-line-end={49}>The following is a tutorial of all commands that I used to crawl with nutch and to connect the solr system to nutch. (Commands are taken from web or slides of lectures).</p>
      <h5 className="code-line" data-line-start={50} data-line-end={51}>Crawl with nutch</h5>
      <p className="has-line-data" data-line-start={51} data-line-end={52}>Go to the website of apache and install the <a href="https://nutch.apache.org/downloads.html">latest binary version of nutch</a>.</p>
      <p className="has-line-data" data-line-start={53} data-line-end={54}>Navigate to the downloaded folder and verify the installation with</p>
      <h6 className="code-line" data-line-start={55} data-line-end={56}><code>./bin/nutch</code></h6>
      <p className="has-line-data" data-line-start={57} data-line-end={58}>If you cannot access the installation run the following commands</p>
      <h6 className="code-line" data-line-start={59} data-line-end={60}><code>chmod +x ./bin/nutch</code></h6>
      <p className="has-line-data" data-line-start={61} data-line-end={62}><strong><em>Note:</em></strong> nutch works only with java version 1.8, therefore before to do the following steps install and put it as default during the project.</p>
      <h5 className="code-line" data-line-start={63} data-line-end={64}>Name your crawler</h5>
      <p className="has-line-data" data-line-start={65} data-line-end={66}>Navigate inside the nutch folder</p>
      <h6 className="code-line" data-line-start={67} data-line-end={68}><code>cd ./conf</code></h6>
      <p className="has-line-data" data-line-start={69} data-line-end={71}>open the <strong><em>nutch-default.xml</em></strong> with your default editor You need to modify the file under the http property that you will find at line 86 (by default).<br />
        <strong><em>Note:</em></strong> DO A COPY OF ALL THE FILE YOU WILL CHANGE</p>
      <pre><code className="has-line-data" data-line-start={73} data-line-end={93}><span className="hljs-tag">&lt;<span className="hljs-title">!−−</span> <span className="hljs-attribute">HTTP</span> <span className="hljs-attribute">properties</span> −−&gt;</span>{"\n"}<span className="hljs-tag">&lt;<span className="hljs-title">property</span>&gt;</span>{"\n"}{"  "}<span className="hljs-tag">&lt;<span className="hljs-title">name</span>&gt;</span>http.agent.name<span className="hljs-tag">&lt;/<span className="hljs-title">name</span>&gt;</span>{"\n"}{"  "}<span className="hljs-tag">&lt;<span className="hljs-title">value</span>&gt;</span>FamousSportPeopleCrawler<span className="hljs-tag">&lt;/<span className="hljs-title">value</span>&gt;</span> <span className="hljs-tag">&lt;<span className="hljs-title">!−−</span> <span className="hljs-attribute">Insert</span> <span className="hljs-attribute">Your</span> <span className="hljs-attribute">crwaler</span> <span className="hljs-attribute">name</span> <span className="hljs-attribute">here</span> −−&gt;</span>{"\n"}{"  "}<span className="hljs-tag">&lt;<span className="hljs-title">description</span>&gt;</span>HTTP 'User-Agent' request header. MUST NOT be empty - {"\n"}{"  "}please set this to a single word uniquely related to your organization.{"\n"}{"\n"}{"  "}NOTE: You should also check other related properties:{"\n"}{"\n"}{"    "}http.robots.agents{"\n"}{"    "}http.agent.description{"\n"}{"    "}http.agent.url{"\n"}{"    "}http.agent.email{"\n"}{"    "}http.agent.version{"\n"}{"\n"}{"  "}and set their values appropriately.{"\n"}{"\n"}{"  "}<span className="hljs-tag">&lt;/<span className="hljs-title">description</span>&gt;</span>{"\n"}<span className="hljs-tag">&lt;/<span className="hljs-title">property</span>&gt;</span>{"\n"}</code></pre>
      <p className="has-line-data" data-line-start={94} data-line-end={95}>After this go back to your root folder.</p>
      <h5 className="code-line" data-line-start={96} data-line-end={97}>Seed Url</h5>
      <p className="has-line-data" data-line-start={98} data-line-end={99}>Now you have to create a seed file that contains all the urls that you want to crawl. Use the following command to create the seed.</p>
      <pre><code className="has-line-data" data-line-start={101} data-line-end={105}>mkdir -p urls{"\n"}cd urls{"\n"}vim seed{"\n"}</code></pre>
      <p className="has-line-data" data-line-start={105} data-line-end={106}>Paste your links into the seed file and save it. For tita use another editor.</p>
      <h5 className="code-line" data-line-start={107} data-line-end={108}>Inject urls into crawler</h5>
      <p className="has-line-data" data-line-start={109} data-line-end={110}>Inject the urls into the Nutch crawl database:</p>
      <h6 className="code-line" data-line-start={111} data-line-end={112}><code>./bin/nutch inject crawl/crawldb urls</code></h6>
      <h5 className="code-line" data-line-start={113} data-line-end={114}>Create a regex rule for crawling</h5>
      <p className="has-line-data" data-line-start={115} data-line-end={116}>You can create your own regular expression. To do that go under</p>
      <h6 className="code-line" data-line-start={117} data-line-end={118}><code>cd ./conf</code></h6>
      <p className="has-line-data" data-line-start={119} data-line-end={120}>and open the file</p>
      <h6 className="code-line" data-line-start={121} data-line-end={122}><code>regex−urlfilter.txt</code></h6>
      <p className="has-line-data" data-line-start={123} data-line-end={124}>On the last line you will find</p>
      <pre><code className="has-line-data" data-line-start={125} data-line-end={128}># accept anything else{"\n"}+.{"\n"}</code></pre>
      <p className="has-line-data" data-line-start={129} data-line-end={130}>Modify this line with one regex</p>
      <h6 className="code-line" data-line-start={131} data-line-end={132}><code>+^(?http|https)\:\/\/(?:www)?\.tio\.ch\</code></h6>
      <h5 className="code-line" data-line-start={133} data-line-end={134}>Fetching</h5>
      <p className="has-line-data" data-line-start={135} data-line-end={136}>You can now generate the database with the comand:</p>
      <h6 className="code-line" data-line-start={137} data-line-end={138}><code>./bin/nutch generate crawl/crawldb crawl/segments</code></h6>
      <p className="has-line-data" data-line-start={139} data-line-end={140}>Fetching the first segment:</p>
      <h6 className="code-line" data-line-start={141} data-line-end={142}><code>./bin/nutch fetch crawl/segments/2*</code></h6>
      <p className="has-line-data" data-line-start={143} data-line-end={144}>Parse the segment</p>
      <h6 className="code-line" data-line-start={145} data-line-end={146}><code>./bin/nutch parse crawl/segments/2*</code></h6>
      <p className="has-line-data" data-line-start={147} data-line-end={148}>Update database</p>
      <h6 className="code-line" data-line-start={149} data-line-end={150}><code>./bin/nutch updatedb crawl/crawldb crawl/segments/2*</code></h6>
      <p className="has-line-data" data-line-start={151} data-line-end={152}>Instead of doing this 4 steps you can run the built-in script of nutch:</p>
      <h6 className="code-line" data-line-start={153} data-line-end={154}><code>./bin/crawl urls &lt;crawl_directory&gt; -&lt;no_of_rounds&gt;</code></h6>
      <h5 className="code-line" data-line-start={155} data-line-end={156}>InvertLinks</h5>
      <h6 className="code-line" data-line-start={157} data-line-end={158}><code>./bin/nutch invertlinks crawl/linkdb crawl/segments/*</code></h6>
      <h4 className="code-line" data-line-start={159} data-line-end={160}>Setup solr for search</h4>
      <p className="has-line-data" data-line-start={161} data-line-end={162}>First <a href="https://www.apache.org/dyn/closer.cgi/lucene/solr/">Download solr binary file</a> and put it near the folder of nucth.</p>
      <p className="has-line-data" data-line-start={163} data-line-end={164}>Navigate inside your solr folder</p>
      <h5 className="code-line" data-line-start={165} data-line-end={166}>Create resources for your core</h5>
      <p className="has-line-data" data-line-start={167} data-line-end={168}>You will copy the default configuration in your core, you can give any name to your folder when copying (in this case nutch).</p>
      <pre><code className="has-line-data" data-line-start={169} data-line-end={172}>mkdir -p ./server/solr/configsets/nutch/{"\n"}cp -r ./server/solr/configsets/_default/* ./server/solr/configsets/nutch/{"\n"}</code></pre>
      <h5 className="code-line" data-line-start={173} data-line-end={174}>Create schema</h5>
      <p className="has-line-data" data-line-start={175} data-line-end={177}>Download <a href="https://github.com/apache/nutch/blob/master/src/plugin/indexer-solr//schema.xml">schema.xml</a>.<br />
        You will put this file inside your core configuration</p>
      <h6 className="code-line" data-line-start={178} data-line-end={179}><code>cp ~/Downloads/schema.xml ./server/solr/configsets/nutch/conf/</code></h6>
      <h5 className="code-line" data-line-start={180} data-line-end={181}>You may need to remove the managed-schema</h5>
      <p className="has-line-data" data-line-start={182} data-line-end={183}>make sure you have no more managed-schema inside you conf core folder.</p>
      <h6 className="code-line" data-line-start={184} data-line-end={185}><code>rm ./server/solr/configsets/nutch/conf/managed-schema</code></h6>
      <h5 className="code-line" data-line-start={186} data-line-end={187}>Create the core</h5>
      <p className="has-line-data" data-line-start={188} data-line-end={189}>Now you are ready to create the core, insead of nutch you will use the name you choosen before.</p>
      <h6 className="code-line" data-line-start={190} data-line-end={191}><code>./bin/solr create -c nutch -d ./server/solr/configsets/nutch/conf/</code></h6>
      <h5 className="code-line" data-line-start={192} data-line-end={193}>Point nutch to solr instance</h5>
      <p className="has-line-data" data-line-start={194} data-line-end={195}>For nutch 1.15 and later edit the file <strong>index-writers.xml</strong> in conf your nutch folder (in apache-nutch not in solr!!!)</p>
      <h6 className="code-line" data-line-start={196} data-line-end={197}><code>cd ./conf</code></h6>
      <p className="has-line-data" data-line-start={198} data-line-end={199}>You will need to modify line 25 (by default) with your core name, in this case nutch</p>
      <pre><code className="has-line-data" data-line-start={201} data-line-end={213}> <span className="hljs-tag">&lt;<span className="hljs-title">writer</span> <span className="hljs-attribute">id</span>=<span className="hljs-value">"indexer_solr_1"</span> <span className="hljs-attribute">class</span>=<span className="hljs-value">"org.apache.nutch.indexwriter.solr.SolrIndexWriter"</span>&gt;</span>{"\n"}{"    "}<span className="hljs-tag">&lt;<span className="hljs-title">parameters</span>&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"type"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">"http"</span>/&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"url"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">"http://localhost:8983/solr/nutch"</span>/&gt;</span>{"  "}<span className="hljs-comment">&lt;!-- modify this value with your core name --&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"collection"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">""</span>/&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"weight.field"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">""</span>/&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"commitSize"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">"1000"</span>/&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"auth"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">"false"</span>/&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"username"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">"username"</span>/&gt;</span>{"\n"}{"      "}<span className="hljs-tag">&lt;<span className="hljs-title">param</span> <span className="hljs-attribute">name</span>=<span className="hljs-value">"password"</span> <span className="hljs-attribute">value</span>=<span className="hljs-value">"password"</span>/&gt;</span>{"\n"}{"    "}<span className="hljs-tag">&lt;/<span className="hljs-title">parameters</span>&gt;</span>{"\n"}</code></pre>
      <h5 className="code-line" data-line-start={214} data-line-end={215}>Pass data from nutch to solr</h5>
      <p className="has-line-data" data-line-start={216} data-line-end={217}>Last but not least you have to pass you data from nutch to solr</p>
      <p className="has-line-data" data-line-start={218} data-line-end={219}>Before it is better that you update the database and invert the links.</p>
      <pre><code className="has-line-data" data-line-start={220} data-line-end={223}>./bin/nutch updatedb crawl/crawldb crawl/segments/2*{"\n"}./bin/nutch invertlinks crawl/linkdb crawl/segments/*{"\n"}</code></pre>
      <p className="has-line-data" data-line-start={224} data-line-end={225}>You can now pass the data, remember to use the right name of the core.</p>
      <pre><code className="has-line-data" data-line-start={227} data-line-end={229}>bin/nutch index -Dsolr.server.url=http://localhost:8983/solr/nutch crawl/crawldb/ -linkdb crawl/linkdb/ crawl/segments/ -dir crawl/segments/20161124145935/ crawl/segments/20161124150145/ -filter -normalize{"\n"}</code></pre>
      <h5 className="code-line" data-line-start={230} data-line-end={231}>Start solr</h5>
      <p className="has-line-data" data-line-start={232} data-line-end={233}>Now you are ready to start solr, navigate to the solr folder and input this command</p>
      <h6 className="code-line" data-line-start={234} data-line-end={235}><code>./bin/solr start</code></h6>
      <p className="has-line-data" data-line-start={236} data-line-end={237}>Now open you browser and navigate to <a href="http://localhost:8983/solr/#/">http://localhost:8983/solr/#/</a></p>
      <p className="has-line-data" data-line-start={238} data-line-end={239}>You should be able to navigate to your core and do query.</p>
    </div>
    );
};

export default Info;
