import { fork, takeLatest } from 'redux-saga/effects';
import getDocumentsData from './getAPI';

function* watcher() {
  yield takeLatest('GET_DOCUMENTS', getDocumentsData);
}

export default function* root() {
  yield fork(watcher);
}
