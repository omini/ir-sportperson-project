
<!-- ## Gitflow

In this project we use Gitflow branching strategy.<br> 
1. Install GitFlow on your machine `$ apt-get install git-flow` 
2. Initialize GitFlow `$ git flow init`
3. Start Developing a new feature `$ git flow feature start [MYFEATURE]`.

For more info check: 
* [Gitflow tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
* [GITflow cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/)
 -->
# Run the frontend application

## Install Yarn

If you don't have yarn installed on your machine, you can find the installation instructions [here](https://yarnpkg.com/en/docs/install#debian-stable)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


# Run the solr system

Navigate to the solr folder.

### `./bin/solr start`

Launches the solr application [htttp://localhost:8983](htttp://localhost:8983), you can access to the core SportPerson to get more 
information and play with the query.


# Cheatsheet of all commands used in this project
The following is a tutorial of all commands that I used to crawl with nutch and to connect the solr system to nutch. (Commands are taken from web or slides of lectures).

## Crawl with nutch
Go to the website of apache and install the [latest binary version of nutch](https://nutch.apache.org/downloads.html). 

Navigate to the downloaded folder and verify the installation with 

### `./bin/nutch`

If you cannot access the installation run the following commands

### `chmod +x ./bin/nutch`

**_Note:_** nutch works only with java version 1.8, therefore before to do the following steps install and put it as default during the project.

## Name your crawler 

Navigate inside the nutch folder

### `cd ./conf`

open the **_nutch-default.xml_** with your default editor You need to modify the file under the http property that you will find at line 86 (by default).
**_Note:_** DO A COPY OF ALL THE FILE YOU WILL CHANGE

```html

<!−− HTTP properties −−>
<property>
  <name>http.agent.name</name>
  <value>FamousSportPeopleCrawler</value> <!−− Insert Your crwaler name here −−>
  <description>HTTP 'User-Agent' request header. MUST NOT be empty - 
  please set this to a single word uniquely related to your organization.

  NOTE: You should also check other related properties:

    http.robots.agents
    http.agent.description
    http.agent.url
    http.agent.email
    http.agent.version

  and set their values appropriately.

  </description>
</property>
```

After this go back to your root folder.

## Seed Url

Now you have to create a seed file that contains all the urls that you want to crawl. Use the following command to create the seed.

```
mkdir -p urls
cd urls
vim seed
```
Paste your links into the seed file and save it. For tita use another editor.

## Inject urls into crawler

Inject the urls into the Nutch crawl database:

### `./bin/nutch inject crawl/crawldb urls`

## Create a regex rule for crawling

You can create your own regular expression. To do that go under

### `cd ./conf`

and open the file

### `regex−urlfilter.txt`

On the last line you will find 
``` 
# accept anything else
+.
```

Modify this line with one regex

### `+^(?http|https)\:\/\/(?:www)?\.tio\.ch\`

**This steps didn't work during the tutorial lecture**

## Fetching 

You can now generate the database with the comand:

### `./bin/nutch generate crawl/crawldb crawl/segments` 

Fetching the first segment:

### `./bin/nutch fetch crawl/segments/2*`

Parse the segment

### `./bin/nutch parse crawl/segments/2*`

Update database

### `./bin/nutch updatedb crawl/crawldb crawl/segments/2*`

Instead of doing this 4 steps you can run the built-in script of nutch:

### `./bin/crawl urls <crawl_directory> -<no_of_rounds>`

## InvertLinks

### `./bin/nutch invertlinks crawl/linkdb crawl/segments/*`

# Setup solr for search

First [Download solr binary file](https://www.apache.org/dyn/closer.cgi/lucene/solr/) and put it near the folder of nucth.

Navigate inside your solr folder

### Create resources for your core

You will copy the default configuration in your core, you can give any name to your folder when copying (in this case nutch).
```
mkdir -p ./server/solr/configsets/nutch/
cp -r ./server/solr/configsets/_default/* ./server/solr/configsets/nutch/
```

### Create schema

Download [schema.xml](https://github.com/apache/nutch/blob/master/src/plugin/indexer-solr//schema.xml).
You will put this file inside your core configuration

### `cp ~/Downloads/schema.xml ./server/solr/configsets/nutch/conf/`

#### You may need to remove the managed-schema

make sure you have no more managed-schema inside you conf core folder.

### `rm ./server/solr/configsets/nutch/conf/managed-schema`

### Create the core

Now you are ready to create the core, insead of nutch you will use the name you choosen before.

### `./bin/solr create -c nutch -d ./server/solr/configsets/nutch/conf/`

### Point nutch to solr instance

For nutch 1.15 and later edit the file **index-writers.xml** in conf your nutch folder (in apache-nutch not in solr!!!)

### `cd ./conf`

You will need to modify line 25 (by default) with your core name, in this case nutch

```html
 <writer id="indexer_solr_1" class="org.apache.nutch.indexwriter.solr.SolrIndexWriter">
    <parameters>
      <param name="type" value="http"/>
      <param name="url" value="http://localhost:8983/solr/nutch"/>  <!-- modify this value with your core name -->
      <param name="collection" value=""/>
      <param name="weight.field" value=""/>
      <param name="commitSize" value="1000"/>
      <param name="auth" value="false"/>
      <param name="username" value="username"/>
      <param name="password" value="password"/>
    </parameters>
```

### Pass data from nutch to solr

Last but not least you have to pass you data from nutch to solr

Before it is better that you update the database and invert the links.
```
./bin/nutch updatedb crawl/crawldb crawl/segments/2*
./bin/nutch invertlinks crawl/linkdb crawl/segments/*
```

You can now pass the data, remember to use the right name of the core.

```
bin/nutch index -Dsolr.server.url=http://localhost:8983/solr/nutch crawl/crawldb/ -linkdb crawl/linkdb/ crawl/segments/ -dir crawl/segments/20161124145935/ crawl/segments/20161124150145/ -filter -normalize
```

### Start solr

Now you are ready to start solr, navigate to the solr folder and input this command

### `./bin/solr start`

Now open you browser and navigate to [http://localhost:8983/solr/#/](http://localhost:8983/solr/#/)

You should be able to navigate to your core and do query.


