export const arrayToObject = array => array.reduce(reducer, {});

const reducer = (obj, item) => {
  obj[item.id] = item;
  return obj;
};
